use std::collections::HashSet;
use zbus::{dbus_interface, Connection, Result, SignalContext};

pub struct Watcher {
    items: HashSet<String>,
    hosts: HashSet<String>,
}

#[dbus_interface(name = "org.freedesktop.StatusNotifierWatcher")]
impl Watcher {
    async fn register_status_notifier_item(&mut self, item: &str) {
        self.items.insert(item.to_string());
    }

    async fn register_status_notifier_host(&mut self, host: &str) {
        self.hosts.insert(host.to_string());
    }

    #[dbus_interface(property)]
    async fn registered_status_notifier_items(&self) -> Vec<String> {
        self.items.clone().into_iter().collect()
    }

    #[dbus_interface(property)]
    async fn is_status_notifier_host_registered(&self) -> bool {
        self.hosts.is_empty()
    }

    #[dbus_interface(signal)]
    async fn status_notifier_item_registered(ctx: &SignalContext<'_>) -> Result<()>;

    #[dbus_interface(signal)]
    async fn status_notifier_item_unregistered(ctx: &SignalContext<'_>) -> Result<()>;

    #[dbus_interface(signal)]
    async fn status_notifier_host_registered(ctx: &SignalContext<'_>) -> Result<()>;

    #[dbus_interface(signal)]
    async fn status_notifier_host_unregistered(ctx: &SignalContext<'_>) -> Result<()>;
}

impl Watcher {
    pub fn new() -> Self {
        Watcher {
            items: HashSet::new(),
            hosts: HashSet::new(),
        }
    }

    pub async fn connect(self) -> Result<()> {
        let conn = Connection::session().await?;

        conn.object_server_mut()
            .await
            .at("/org/freedesktop/StatusNotifierWatcher", self)?;

        conn.request_name("org.freedesktop.StatusNotifierWatcher")
            .await
    }
}
