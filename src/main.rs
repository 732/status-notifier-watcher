mod watcher;

use watcher::Watcher;
use zbus::{Connection, Result};

#[async_std::main]
async fn main() -> Result<()> {
    let watcher = Watcher::new();

    // let conn = Connection::session().await?;

    // conn.object_server_mut()
    //     .await
    //     .at("/org/freedesktop/StatusNotifierWatcher", watcher)?;

    // conn.request_name("org.freedesktop.StatusNotifierWatcher")
    //     .await?;

    watcher.connect().await?;

    loop {
        std::thread::park();
    }
}
